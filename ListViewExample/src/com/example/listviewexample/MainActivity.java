package com.example.listviewexample;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnItemClickListener{
ListView listview;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		listview =(ListView) findViewById(R.id.listView);
		listview.setOnItemClickListener(this);
		String arrayItem[] = new String[] { "Android Introduction","Android Setup/Installation","Android Hello World","Android Layouts/Viewgroups","Android Activity & Lifecycle","Intents in Android","Services","BroadCastReceivers","","",""};
		ArrayAdapter<String> adapter= new ArrayAdapter<>(getApplicationContext(),android.R.layout.test_list_item, arrayItem);
		listview.setAdapter(adapter);
	}
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Toast.makeText(getApplicationContext(), "Clicked on Item "+ position, Toast.LENGTH_SHORT).show();
		
	}

	
}
